<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'F:/Tutorials/Grav/system/blueprints/config/streams.yaml',
    'modified' => 1458722906,
    'data' => [
        'title' => 'PLUGIN_ADMIN.FILE_STREAMS',
        'form' => [
            'validation' => 'loose',
            'hidden' => true,
            'fields' => [
                'schemes.xxx' => [
                    'type' => 'array'
                ]
            ]
        ]
    ]
];
